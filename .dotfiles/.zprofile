# Homebrew
eval "$(/opt/homebrew/bin/brew shellenv)"

if [ -d "/opt/homebrew/opt/ruby/bin" ]; then
  export PATH=/opt/homebrew/opt/ruby/bin:$PATH
  export PATH=`gem environment gemdir`/bin:$PATH
fi

export HOMEBREW_BUNDLE_FILE=~/repos/mac-setup/.dotfiles/Brewfile