#!/usr/bin/env zsh

clear
echo "-----------------"
echo "NM's setup script"
echo "Version 0.5"
echo "-----------------"

echo "+----------------------------------+"
echo "Grabbing mac-setup repo from GitLab."
echo "+----------------------------------+"
if [ ! -d $HOME/mac-setup ]
then
    echo "→ Bad news. mac-setup repo is not found on your computer. Grabbing.\n"
    git clone https://gitlab.com/nvmfst/mac-setup.git
else
    echo "→ Nevermind. mac-setup repo is already on your computer.\n"
fi

echo "+---------------------------------+"
echo "Adding aliases from mac-setup repo."
echo "+---------------------------------+"
ln -s -f "$HOME/mac-setup/.dotfiles/.hushlogin" $HOME/.hushlogin
ln -s -f "$HOME/mac-setup/.dotfiles/.zshrc" $HOME/.zshrc
ln -s -f "$HOME/mac-setup/.dotfiles/.zprofile" $HOME/.zprofile

echo "→ Aliases added and now sourcing them.\n"

source $HOME/.hushlogin
source $HOME/.zprofile
source $HOME/.zshrc

echo "+----------------------------+"
echo "Grabbing Homebrew from GitHub."
echo "+----------------------------+"
if [ ! $(which brew) ]
then
    echo "→ Bad news. Homebrew is not found on your computer. Grabbing.\n"
    /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
else
    echo "→ Nevermind. Homebrew is already on your computer.\n"
fi

echo "+------------------------------------+"
echo "Making sure Homebrew is ready to brew."
echo "+------------------------------------+"
eval $(/opt/homebrew/bin/brew shellenv)
brew doctor
brew upgrade
brew cleanup
echo "→ Moving on...\n"

echo "+----------------+"
echo "Getting git ready."
echo "+----------------+"
git config --global user.name "Jack DeCarlo"
git config --global user.email "gdhamell@fastmail.com"
echo "→ Git is ready.\n"

echo "+----------------+"
echo "Grabbing gem file for Don Melton's Video Transcoding tools."
echo "+----------------+"
gem install video_transcoding
echo "→ Gem installed.\n"

echo "+----------------+"
echo "Customizing macOS"
echo "+----------------+"
echo "(1) Configuring the Dock settings..."
defaults write com.apple.dock show-recents -bool false
defaults write com.apple.dock static-only -bool true
defaults write com.apple.dock "tilesize" -int "40" && killall Dock

echo "(2) Configuring Screenshot settings..."
defaults write com.apple.screencapture "show-thumbnail" -bool "false"
defaults write com.apple.screencapture disable-shadow -bool true
defaults write com.apple.screencapture "location" -string "~/Documents/Screenshots" && killall SystemUIServer

echo "(3) Configuring Finder settings..."
defaults write NSGlobalDomain "AppleShowAllExtensions" -bool "true"
defaults write com.apple.finder "ShowPathbar" -bool "true"
defaults write com.apple.finder "FXPreferredViewStyle" -string "Nlsv"
defaults write com.apple.finder "_FXSortFoldersFirst" -bool "true"
defaults write com.apple.finder "FXDefaultSearchScope" -string "SCcf"
defaults write com.apple.finder "FXRemoveOldTrashItems" -bool "true"
defaults write com.apple.finder "FXEnableExtensionChangeWarning" -bool "false"
defaults write NSGlobalDomain "NSDocumentSaveNewDocumentsToCloud" -bool "false" && killall Finder

echo "(4) Configuring Desktop settings..."
defaults write com.apple.finder "_FXSortFoldersFirstOnDesktop" -bool "true"
defaults write com.apple.finder "ShowHardDrivesOnDesktop" -bool "true"
defaults write com.apple.finder "ShowExternalHardDrivesOnDesktop" -bool "true" && killall Finder

echo "(5) Configuring Menu Bar settings..."
defaults write com.apple.menuextra.clock "DateFormat" -string "\"EEE d MMM HH:mm\"" 
defaults write com.apple.Siri StatusMenuVisible -bool false

echo "(6) Configuring Keyboard settings..."
defaults write -g NSAutomaticSpellingCorrectionEnabled -bool false
defaults write NSGlobalDomain NSAutomaticCapitalizationEnabled -bool false
defaults write NSGlobalDomain NSAutomaticDashSubstitutionEnabled -bool false
defaults write NSGlobalDomain NSAutomaticQuoteSubstitutionEnabled -bool false
defaults write NSGlobalDomain NSAutomaticPeriodSubstitutionEnabled -bool false